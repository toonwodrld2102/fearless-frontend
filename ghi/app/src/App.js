import logo from './logo.svg';
import './App.css';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendeesForm from './AttendeesForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';
import {BrowserRouter, Route, Routes} from 'react-router-dom';


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage/>}/>
          <Route path="presentations">
            <Route path='new' element={< PresentationForm />} />
          </Route>
          <Route path="attendees">
            <Route index element={<AttendeesList attendees={props.attendees} />} />
            <Route path="new" element={< AttendeesForm />}/>
          </Route>
          <Route path='conferences'>
            <Route path="new" element={< ConferenceForm />} />
          </Route>
          <Route path='locations'>
            <Route path="new" element={ <LocationForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
    </>
  );
}

export default App;
