import React, {useEffect, useState} from 'react';

function AttendeesForm(props) {
  const [conferences, setConferences] = useState([]);
  const [conference, setConference] = useState('')
  const [email, setEmail] = useState('');
  const [name, setName] = useState('');
  const handleConferenceChange = (event) => {
    const value = event.target.value;
    setConference(value);
  }
  const handleEmailChange = (event) => {
    const value = event.target.value;
    setEmail(value);
  }
  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  }

  const handleSubmit = async(event) => {
    event.preventDefault();

    // create an empty JSON object
    const data = {};

    data.name = name;
    data.email = email;
    data.conference = conference;

    console.log(data);

    const attendeeUrl = `http://localhost:8001${conference}attendees/`;
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(attendeeUrl, fetchConfig);
    if (response.ok) {
      const newConference = await response.json();
      console.log(newConference);

      setName('');
      setConference('');
      setEmail('');
    }
  }

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/conferences/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences)
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

    return (
        <div className="my-5 container">
          <div className="col col-sm-auto">
            <img width="600" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg"/>
          </div>
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create an attendee</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Name" name='name' value={name}required type="text" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEmailChange} placeholder="Email" name='email' value={email} required type="email" id="email" className="form-control"/>
                <label htmlFor="email">Email</label>
              </div>
              <div className="mb-3">
                <select required name="Conferences" onChange={handleConferenceChange} value={conference} id="conference" className="form-select">
                  <option value="">Choose a Conference</option>
                  {conferences.map(conference => {
                    return (
                      <option key={conference.href} value={conference.href}>
                      {conference.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default AttendeesForm;
