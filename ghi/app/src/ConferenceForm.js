import React, {useEffect, useState} from 'react';

function ConferenceForm (props) {
    const [locations, setLocations] = useState([]);
    const [name, setName] = useState('');
    const [starts, setStart] = useState('');
    const [ends, setEnd] = useState('');
    const [description, setDescription] = useState('');
    const [max_presentations, setMaxp] = useState('');
    const [max_attendees, setMaxA] = useState('');
    const [location, setLocation] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleStartChange = (event) => {
        const value = event.target.value;
        setStart(value);
    }
    const handleEndChange = (event) => {
        const value = event.target.value;
        setEnd(value);
    }

    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const handlePresentationChange = (event) => {
        const value = event.target.value;
        setMaxp(value);
    }

    const handleAttendeeChange = (event) => {
        const value = event.target.value;
        setMaxA(value);
    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }


    const handleSubmit = async(event) => {
      event.preventDefault();

      // create an empty JSON object
      const data = {};

      data.name = name;
      data.starts = starts;
      data.ends = ends;
      data.description = description;
      data.max_presentations = max_presentations;
      data.max_attendees = max_attendees;
      data.location = location;

      console.log(data);

      const conferenceUrl = 'http://localhost:8000/api/conferences/';
      const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
              'Content-Type': 'application/json',
          },
        };
      const response = await fetch(conferenceUrl, fetchConfig);
      if (response.ok) {
        const newConference = await response.json();
        console.log(newConference);

        setName('');
        setStart('');
        setEnd('');
        setDescription('');
        setMaxp('');
        setMaxA('');
        setLocation('');
      }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setLocations(data.locations)
        }
    }


    useEffect (() => {
        fetchData();
    }, []);

    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new conference</h1>
              <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                  <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" id="name" className="form-control"/>
                  <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleStartChange} value={starts} placeholder="Start" required type="date" id="start" className="form-control"/>
                  <label htmlFor="start">Start</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleEndChange} value={ends} placeholder="End" required type="date" id="end" className="form-control"/>
                  <label htmlFor="end">End</label>
                </div>
                <div className="form-floating mb-3">
                  <textarea onChange={handleDescriptionChange} value={description} placeholder="Description" required type="text" id="description" className="form-control"></textarea>
                  <label htmlFor="description">Description</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handlePresentationChange} value={max_presentations} placeholder="Max Presentations" required type="number" id="max_presentations" className="form-control"/>
                  <label htmlFor="max_presentations">Max Presentations</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleAttendeeChange} value={max_attendees} placeholder="Max Attendees" required type="number" id="max_attendees" className="form-control"/>
                  <label htmlFor="max_attendees">Max Attendees</label>
                </div>
                <div className="mb-3">
                  <select required onChange={handleLocationChange} value={location} id="location" className="form-select">
                    <option value="">Choose a location</option>
                    {locations.map(location => {
                        return (
                            <option key={location.name} value={location.id}>
                            {location.name}
                            </option>
                        );
                  })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
}

export default ConferenceForm;
