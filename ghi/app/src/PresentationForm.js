import React, {useEffect, useState} from 'react';

function PresentationForm(props) {
  const [conferences, setConferences] = useState([]);
  const [presenter_name, setPName] = useState('');
  const [presenter_email, setPEmail] = useState('');
  const [company_name, setCName] = useState('');
  const [title, setTitle] = useState('');
  const [synopsis, setSynopsis] = useState('');
  const [conference, setConference] = useState('');

  const handlePNameChange = (event) => {
    const value = event.target.value;
    setPName(value);
  }
  const handlePEmailChange = (event) => {
    const value = event.target.value;
    setPEmail(value);
  }
  const handleCNameChange = (event) => {
    const value = event.target.value;
    setCName(value);
  }
  const handleTitleChange = (event) => {
    const value = event.target.value;
    setTitle(value);
  }
  const handleSynposisChange = (event) => {
    const value = event.target.value;
    setSynopsis(value);
  }
  const handleConferenceChange = (event) => {
    const value = event.target.value;
    setConference(value);
  }

  const handleSubmit = async(event) => {
    event.preventDefault();

    // create an empty JSON object
    const data = {};

    data.presenter_name = presenter_name;
    data.presenter_email = presenter_email;
    data.company_name = company_name;
    data.title = title;
    data.synopsis = synopsis;
    data.conference = conference;

    console.log(data);

    const PresentationUrl = `http://localhost:8000${conference}presentations/`;
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(PresentationUrl, fetchConfig);
    if (response.ok) {
      const newPresentation = await response.json();
      console.log(newPresentation);

      setPName('');
      setPEmail('');
      setCName('');
      setTitle('');
      setSynopsis('');
      setConference('');
    }
  }

  const fetchData = async () => {
    const url = `http://localhost:8000/api/conferences/`;

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences)
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new presentation</h1>
              <form onSubmit={handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                  <input onChange={handlePNameChange} placeholder="Presentation Name" name='presentationName' value={presenter_name} required type="text" id="presenterName" className="form-control"/>
                  <label htmlFor="name">Presenter Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handlePEmailChange} placeholder="Presentation Email" name='presentationEmail' value={presenter_email} required type="email" id="email" className="form-control"/>
                  <label htmlFor="email">Presenter Email</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleCNameChange} placeholder="Company Name" name='companyName' value={company_name} required type="text" id="companyName" className="form-control"/>
                  <label htmlFor="company_name">Company Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleTitleChange} placeholder="Title" name='title' value={title} required type="text" id="title" className="form-control"/>
                  <label htmlFor="title">Title</label>
                </div>
                <div className="form-floating mb-3">
                  <textarea onChange={handleSynposisChange} placeholder="Synopsis" name='synopsis' value={synopsis} required type="text" id="synopsis" className="form-control"></textarea>
                  <label htmlFor="synopsis">Synopsis</label>
                </div>
                <div className="mb-3">
                    <select required name="Conferences" onChange={handleConferenceChange} value={conference} id="conference" className="form-select">
                        <option value="">Choose a Conference</option>
                        {conferences.map(conference => {
                            return (
                                <option key={conference.href} value={conference.href}>
                                {conference.name}
                                </option>
                            );
                          })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
}

export default PresentationForm;
